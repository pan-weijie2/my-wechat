// pages/category/category.js
const app=getApp();
const { updateCart } = require("../../utils/util");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cateList:[],
    goodsList:[],
    goodsNum:1
  },

  showGoods(e){
    let _this=this
    let index = e.detail;
    let cateCode = this.data.cateList[index].cateCode;
    console.log(cateCode)
    wx.request({
      url: app.globalData.remoteUrl+'GetGoodsListByCate', //仅为示例，并非真实的接口地址
      data: {
        token:"kjxy_Interface_2024",
        cateCode:cateCode
      },
      method:"POST",
      success (res) {
        console.log(res)
        _this.setData({
          goodsList:res.data.goodsList
        })
      }
    })
  },

  setNum(e){
    console.log(e)
    this.setData({goodsNum:e.detail})
  },

  add2Cart(e){
    let index = e.currentTarget.dataset.index;
    let currentGoods = this.data.goodsList[index];
    let buyGoods = {id:currentGoods.id,goodsName:currentGoods.goodsName,goodsImage:currentGoods.goodsImage,num:this.data.goodsNum,price:currentGoods.goodsPrice};
    app.globalData.cateData = updateCart(app.globalData.cartData,buyGoods)
    this.setData({goodsNum:1});
    wx.setTabBarBadge({
      index: 2,
      text: app.globalData.cartData.length+"",
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({cateList:app.globalData.cateList})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // let cateCode = wx.getStorageSync('cateCode')
    let cateCode = app.globalData.currentCateCode;
    let _this=this
    wx.request({
      url: app.globalData.remoteUrl+'GetGoodsListByCate', //仅为示例，并非真实的接口地址
      data: {
        token:"kjxy_Interface_2024",
        cateCode:cateCode
      },
      method:"POST",
      success (res) {
        console.log(res)
        _this.setData({
          goodsList:res.data.goodsList
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})